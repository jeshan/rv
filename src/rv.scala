/*
 * The "Simplified" BSD Licence follows:
 *
 * Copyright (c) 2012, Jeshan G. BABOOA (mailto:j@code.jeshan.co)
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * This software is provided by the copyright holders and contributors "as is"
 * and any express or implied warranties, including, but not limited to, the
 * implied warranties of merchantability and fitness for a particular purpose
 * are disclaimed. In no event shall the copyright holder or contributors be
 * liable for any direct, indirect, incidental, special, exemplary, or consequential
 * damages (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict liability,
 * or tort (including negligence or otherwise) arising in any way out of the
 * use of this software, even if advised of the possibility of such damage.
 */

import io.Source
import java.io.{FileWriter, BufferedWriter, File}
import java.math.BigInteger
import java.net.URI
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.regex.Pattern
import java.util.{TimeZone, Date}
import org.apache.commons.io.{FilenameUtils, FileUtils}
import org.jsoup.Jsoup
import org.jsoup.nodes.{Element, Document}
import scala.collection.JavaConverters._

/**
 * User: jeshan
 * Date: 20/12/12
 * Time: 22:56
 */
object rv {

    val imageExtensions = Array("png", "gif", "jpg", "jpeg")

    val selectorMap = Map("css" ->("link[type=text/css]", "href", "<link rel=\"stylesheet\" href=\"%s\" />"),
        "js" ->("script[type=text/javascript]", "src", "<script type=\"text/javascript\" src=\"%s\"></script>"))

    val imageSelectors = Array(("img", "src"))

    val htmlExtensions = Array("html", "htm")

    val filePrintfFormat = "%s.v%s.%s"

    val iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH'h'mm'm'ss")

    iso8601Format.setTimeZone(TimeZone.getTimeZone("UTC"))

    val md5Function = (file: File) => new BigInteger(1, md5.digest(FileUtils.readFileToByteArray(file))).toString(16)
    val iso8601Function = (file: File) => iso8601Format.format(new Date(file.lastModified()))

    val fileRegexFormats = {
        Array(("iso8601", "(.+)\\.v.{17,}\\..+", iso8601Function), ("md5", ".+\\.v.{32}\\..+",
            md5Function)).view
    }

    val md5 = MessageDigest.getInstance("MD5")

    val selectedVersionFormat = "iso8601"

    val fileRegexFormatTuple = fileRegexFormats.find(p => p._1.equals(selectedVersionFormat)).get

    val fileRegexFormat = Pattern.compile(fileRegexFormatTuple._2)

    val supportedVersionFormatNames = fileRegexFormats.map(x => x._1)

    val otherSupportedVersionFormats = fileRegexFormats.filter(p => p._1.equals(selectedVersionFormat)).map(x => x
        ._2)

    val filter = "*/lib/*"

    val ignoreSchemes = Array("http", "https")

    var removeStamp = false

    var squisherMap: scala.collection.mutable.Map[String, (BufferedWriter, File)] = null

    def isSquish = squisherMap != null

    def processFile(file: File, tag: Element, attrName: String, attrValue: String) {
        val name = file.getName
        val matcher = fileRegexFormat.matcher(name)
        var nameWithoutExtension: String = null
        if (matcher.find()) {
            nameWithoutExtension = matcher.group(1)
        }
        else {
            nameWithoutExtension = FilenameUtils.removeExtension(name)
        }
        var newName: String = null
        if (removeStamp) {
            newName = "%s.%s".format(nameWithoutExtension, FilenameUtils.getExtension(name))
        }
        else {
            newName = String.format(filePrintfFormat, nameWithoutExtension, fileRegexFormatTuple._3(file),
                FilenameUtils.getExtension(name))
        }
        if (!newName.equals(name)) {
            val newFile = new File(file.getParent, newName)
            file.renameTo(newFile)
            tag.attr(attrName, attrValue.substring(0, attrValue.lastIndexOf(name)) + newName)
        }
    }

    def updateHtml(file: File, document: Document) {
        val html = document.html()
        val writer = new BufferedWriter(new FileWriter(file))
        writer.write(html)
        writer.close()
    }

    def process(resourceType: String, tag: Element, attrName: String, attrValue: String, file: File,
                document: Document) {
        val resourceFile = new File(file.getParent, attrValue)
        if (resourceType != null && isSquish) {
            tag.remove()
            val writer = squisherWriterFor(resourceType, file)
            Source.fromFile(resourceFile).getLines().foreach(x => {
                writer.write(x)
                writer.newLine()
            })
        }
        else {
            if (!FilenameUtils.wildcardMatchOnSystem(resourceFile.getPath, filter)) {
                processFile(resourceFile, tag, attrName, attrValue)
            }
        }
    }

    def processForSquish(document: Document, newDirectory: String) {
        if (!isSquish) {
            return
        }
        squisherMap.foreach(x => {
            val writer = x._2._1
            writer.close()
            val file = x._2._2
            val resourceType = x._1.takeWhile(x => !x.equals(':'))
            val newRelativePath = resourceType + System.getProperty("file.separator") +
                fileRegexFormatTuple._3(file) + "." + resourceType
            val newFile = new File(newDirectory, newRelativePath)
            file.renameTo(newFile)
            document.head().append(selectorMap.get(resourceType).get._3.format(newRelativePath))
        })
        squisherMap.clear()
    }

    def main(args: Array[String]) {
        args.foreach(arg => {
            if (arg.equalsIgnoreCase("--removeStamp")) {
                removeStamp = true
            }
            if (arg.equalsIgnoreCase("--squish")) {
                //squishWriter = new BufferedWriter(new FileWriter())
                squisherMap = scala.collection.mutable.Map.empty
            }
        })
        val directory = new File(args.filterNot(x => x.startsWith("--")).head)
        val files = FileUtils.listFiles(directory, htmlExtensions, true).asScala
        files.foreach(file => {
            val document = Jsoup.parse(file, "UTF-8")
            selectorMap.foreach(entry => {
                val selector = entry._2._1
                val attrName = entry._2._2
                val selected = document.select(selector).asScala
                selected.foreach(tag => {
                    val attrValue = tag.attr(attrName)
                    if (!ignoreTag(tag, attrValue)) {
                        process(entry._1, tag, attrName, attrValue, file, document)
                    }
                })
            })
            imageSelectors.foreach(tuple => {
                val attrName = tuple._2
                document.select(tuple._1).asScala.foreach(tag => {
                    val attrValue = tag.attr(attrName)
                    if (!ignoreTag(tag, attrValue)) {
                        process(null, tag, attrName, attrValue, file, document)
                    }
                })
            })
            processForSquish(document, file.getParent)
            updateHtml(file, document)
        })
    }

    def ignoreTag(tag: Element, attrValue: String): Boolean = {
        if (ignoreSchemes.contains(new URI(attrValue).getScheme)) {
            return true
        }
        false
    }

    def squisherWriterFor(resourceType: String, file: File) = {
        val key = resourceType + ":" + file
        val option = squisherMap.get(key)
        if (option.isDefined) {
            option.get._1
        }
        else {
            val file = File.createTempFile("rv-" + resourceType, "." + resourceType)
            file.deleteOnExit()
            val writer = new BufferedWriter(new FileWriter(file))
            squisherMap.put(key, (writer, file))
            writer
        }
    }
}
